#!/bin/sh -e

DISK=$1

[ "$EUID" -eq 0 ] || { echo "Please run as root"; exit; }
[ -b "$DISK" ] || { "Please supply sdcard disk"; exit; }

set -x

dd if=sysimage-sdcard.img of=${DISK} bs=1M conv=sync
parted ${DISK} -fs resizepart 5 '100%'
partprobe ${DISK}
sleep 1
e2fsck -f ${DISK}5
resize2fs ${DISK}5
