#!/bin/sh
[ "$EUID" -eq 0 ] || { echo "Please run as root"; exit; }

podman image exists debootstrapper || podman build .
podman run --privileged -v /mnt/root:/mnt/root:Z -it debootstrapper:latest /bin/bash \
    -c "debootstrap --arch=riscv64 unstable /mnt/root/ || cat ./mnt/root/debootstrap/debootstrap.log"
